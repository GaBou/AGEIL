from django.shortcuts import render

# Create your views here.
def home(request):
	
	return render(request, 'home/accueil.html')

def travaux(request):
	
	return render(request, 'pageTravaux.html')