from django.conf.urls import url
from . import views

urlpatterns = [
	url(r'preJury$', views.preJury),
	url(r'jury$', views.jury),
	url(r'PV$', views.pv),
	url(r'choixSemestreAReporter$', views.choixSemestreAReporter),
	url(r'reporterResultat$', views.reporterResultat),
	url(r'modifierResultat/(?P<id>\d+)$', views.modifierResultat),
	url(r'modifierDevenir/(?P<id>\d+)$', views.modifierDevenir),
	url(r'validerSemestre/(?P<id>\d+)$', views.validerSemestre),
]